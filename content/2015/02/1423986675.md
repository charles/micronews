Title: Come squash some bugs in Portland
Slug: 1423986675
Date: 2015-02-15 07:51:15
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/fKugsTEARxGX521LcmZkFw

Bug squashing party in <a href="https://wiki.debian.org/BSP/2015/02/us/Portland">Portland on 2015-02-21</a>! Please join us in adventures leading to the Jessie release!

