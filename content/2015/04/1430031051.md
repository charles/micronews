Title: Releasing Debian jessie: jessie-backports
Slug: 1430031051
Date: 2015-04-26 06:50:51
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/eNTC2jRzTwW8fFsQ_NymzQ

The jessie-backports <a href="https://lists.debian.org/debian-backports-announce/2015/04/msg00000.html">opened</a>, providing packages taken from Debian testing, adjusted and recompiled to Debian Jessie. #releasingjessie

