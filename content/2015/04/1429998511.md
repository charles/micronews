Title: Releasing Debian jessie: contributing on mailing lists
Slug: 1429998511
Date: 2015-04-25 21:48:31
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/QqjtxKhKQ6eA2_BtrsD0TA

You don't need to be an expert to contribute to Debian! Assisting users with problems on the user support lists is a large contribution to Debian. #releasingjessie

