Title: Releasing Debian jessie: news sites
Slug: 1430051489
Date: 2015-04-26 12:31:29
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/l9n8PmN1TN2bdR1OOjmR7A

News of the jessie release has been shared on <a href="https://lwn.net/Articles/641875/">LWN</a>, <a href="http://distrowatch.com/8914">Distrowatch</a>, <a href="http://linux.slashdot.org/story/15/04/26/0322241/debian-8-jessie-released/">Slashdot</a>, <a href="https://news.ycombinator.com/item?id=9440386">Hacker News</a> and <a href="http://www.phoronix.com/scan.php?page=news_item&amp;px=Debian-8-Jessie-Released">Phoronix</a>, thanks! #releasingjessie <br />

