Title: Releasing Debian: UEFI support.
Slug: 1430020450
Date: 2015-04-26 03:54:10
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/LKVIh20TRJibkuD1MGiXuQ

The Jessie installer improves support UEFI fireware and supports installing on 32-bit UEFI firmware with a 64-bit kernel.

