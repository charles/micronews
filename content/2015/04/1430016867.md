Title: Releasing Debian jessie: A number of source packages.
Slug: 1430016867
Date: 2015-04-26 02:54:27
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/Rakw_vypSsK8bg0Y9NVgGw

Jessie has 21,087 source packages with 8,359,525 source files. See the stats <a href="http://sources.debian.net/stats/jessie/ #releasingjessie">here</a> .

