Title: Releasing Debian jessie: Available support
Slug: 1429984824
Date: 2015-04-25 18:00:24
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/i3IM-na8Qqa0duRliZoCgg

Support for Debian is available via documentation, wiki, forums, mailing lists, newsgroups, the bug tracker, chat, consultants and more. <a href="https://www.debian.org/support">https://www.debian.org/support</a>

