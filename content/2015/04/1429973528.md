Title: Releasing Debian jessie: Debian machines
Slug: 1429973528
Date: 2015-04-25 14:52:08
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/ssnetZ6_Q5-oLsnyIUpjRQ

The Debian sysadmins look after <a href="https://db.debian.org/machines.cgi">201 virtual and physical machines</a> running services for Debian. #releasingjessie

