Title: Releasing Debian jessie: Upgrading advice link.
Slug: 1430018893
Date: 2015-04-26 03:28:13
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/1kFDSciYTEmpPp8ImXt8iA

Be sure to check out the upgrading <a href="https://www.debian.org/releases/jessie/amd64/release-notes/ch-upgrading.en.html">advice</a>  for troubleshooting and warnings prior to your upgrade! <br />

