Title: Voting has officially started for the next Debian Public Leader.
Slug: 1427849202
Date: 2015-04-01 00:46:42
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/2BQySZXhS4euJxYJyHgRiw

Following nominations and Q&amp;As, voting for the next Debian Project Leader has <a href="https://lists.debian.org/debian-vote/2015/03/msg00122.html">officially started</a> .

