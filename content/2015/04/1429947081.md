Title: Releasing Debian jessie: short freeze!
Slug: 1429947081
Date: 2015-04-25 07:31:21
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/z2jaYh91Tmq1yJYz6_KPwA

Debian jessie is the second release since sarge to have a *shorter* freeze than 6 months (the other one being squeeze at one day shy of 6 months).  #releasingjessie

