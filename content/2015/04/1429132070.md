Title: Neil McGovern wins  the 2015 Debian Project Leader election
Slug: 1429132070
Date: 2015-04-15 21:07:50
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/Vesr4AlIQs-o0rSZswHTUA

Neil McGovern <a href="https://vote.debian.org/~secretary/leader2015/">wins</a> the 2015 Debian Project Leader election, term to start 2015-04-17. Congratulations, Neil, and thanks to Mehdi and Gergely.

