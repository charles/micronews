Title: Releasing Debian jessie: security team
Slug: 1429976854
Date: 2015-04-25 15:47:34
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/xba4jmfXTeqf4SkRae7cFA

The <a href="https://www.debian.org/security/">Debian security team</a> has handled at least 3229 advisories. #releasingjessie

