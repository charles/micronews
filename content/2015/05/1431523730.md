Title: Debian Project News 2015-05-12
Slug: 1431523730
Date: 2015-05-13 13:28:50
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/barqWt9UQNq50P40PAmx7w

Debian Project News 2015-05-12: Debian 8 Jessie released, Live coverage, DPL election, Outreach Delegation, 2015 Google Summer of Code, Long Term Support, Reports, Bucharest miniDebConf, Interviews with Lucas Nussbaum and Shirish Agarwal <a href="https://www.debian.org/News/weekly/2015/04/">https://www.debian.org/News/weekly/2015/04/</a>

