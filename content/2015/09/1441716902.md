Title: Make software trustworthy again - Debian reproducible builds
Slug: 1441716902
Date: 2015-09-08 12:55:02
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/UIYkjLqWQuCTP77kp08vaA

Article in <a href="http://motherboard.vice.com/read/how-debian-is-trying-to-shut-down-the-cia-and-make-software-trustworthy-again">Vice's Motherboard tech site about making software trustworthy again with Debian reproducible builds</a> 

