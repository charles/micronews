Title: None
Slug: 1441283767
Date: 2015-09-03 12:36:07
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/0V75h93dTR2uiGRoh-Z7XQ

Thanks to Ben Hutchings' work, the Debian package for the <a href="https://packages.debian.org/source/experimental/linux">linux</a> kernel is now tested as <a href="https://reproducible.debian.net/rb-pkg/experimental/amd64/linux.html">building reproducibly</a>.

