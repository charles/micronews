Title: If you use Debian container images, please note that "debian:bookworm" images are already using deb822-style repository sources https://manpages.debian.org/bullseye/apt/sources.list.5.en.html#DEB822-STYLE_FORMAT
Slug: 1676642270
Date: 2023-02-17 13:57
Author: Laura Arjona Reina
Status: published

If you use Debian container images, please note that "debian:bookworm" images are already using deb822-style repository sources [https://manpages.debian.org/bullseye/apt/sources.list.5.en.html#DEB822-STYLE_FORMAT](https://manpages.debian.org/bullseye/apt/sources.list.5.en.html#DEB822-STYLE_FORMAT)
