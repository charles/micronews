Title: We don't wish to ruin our own surprise anniversary party, but we'd like to share that we celebrate 30 years of Debian on August 16 of this year. Join us! Plenty of Cheese and Cake to be had. :) Organize your very own Debian Day! https://wiki.debian.org/DebianDay/2023 #debian #debianday
Slug: 1691034469
Date: 2023-08-03 03:47
Author: Donald Norwood
Status: published

We don't wish to ruin our own surprise anniversary party, but we'd like to share that we celebrate 30 years of Debian on August 16 of this year. Join us! Plenty of Cheese and Cake to be had. :) Organize your very own Debian Day! [https://wiki.debian.org/DebianDay/2023](https://wiki.debian.org/DebianDay/2023) #debian #debianday
