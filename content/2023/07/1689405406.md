Title: Gomuks (Terminal based Matrix client written in Go) accepted into unstable https://tracker.debian.org/news/1442864/accepted-gomuks-030-1-source-amd64-into-unstable/
Slug: 1689405406
Date: 2023-07-15 07:16
Author: Nilesh Patra
Status: published

Gomuks (Terminal based Matrix client written in Go) accepted into unstable [https://tracker.debian.org/news/1442864/accepted-gomuks-030-1-source-amd64-into-unstable/](https://tracker.debian.org/news/1442864/accepted-gomuks-030-1-source-amd64-into-unstable/)
