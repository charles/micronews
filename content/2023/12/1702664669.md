Title: [SUA 247-1] Updated Linux packages: The Linux Kernel update included with Debian 12.4 introduced a regression with some WiFi adapters. A new update resolves that regression. You can get the updated packages by adding the stable-updates archive for your distribution to your /etc/apt/sources.list. https://lists.debian.org/debian-stable-announce/2023/12/msg00002.html #debian
Slug: 1702664669
Date: 2023-12-15 18:24
Author: Donald Norwood
Status: published

[SUA 247-1] Updated Linux packages: The Linux Kernel update included with Debian 12.4 introduced a regression with some WiFi adapters. A new update resolves that regression. You can get the updated packages by adding the stable-updates archive for your distribution to your /etc/apt/sources.list. [https://lists.debian.org/debian-stable-announce/2023/12/msg00002.html](https://lists.debian.org/debian-stable-announce/2023/12/msg00002.html) #debian
