Title: DebCamp starts today! https://wiki.debian.org/DebConf/23/DebCamp #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia
Slug: 1693731518
Date: 2023-09-03 08:58
Author: Anupa Ann Joseph
Status: published

DebCamp starts today! [https://wiki.debian.org/DebConf/23/DebCamp](https://wiki.debian.org/DebConf/23/DebCamp) #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia
