Title: If you plan to participe to the DebConf23 Key Signing party, please read the final instructions! https://lists.debian.org/debconf-announce/2023/09/msg00007.html #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia
Slug: 1694135567
Date: 2023-09-08 01:12
Author: Jean-Pierre Giraud
Status: published

If you plan to participe in the DebConf23 Key Signing party, please read the final instructions! [https://lists.debian.org/debconf-announce/2023/09/msg00007.html](https://lists.debian.org/debconf-announce/2023/09/msg00007.html) #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia
