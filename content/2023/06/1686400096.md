Title: The contents of bookworm are now being sent over to the Debian Images team for building and testing of all the .iso files you will be able to download when the release is complete #ReleasingDebianBookworm
Slug: 1686400096
Date: 2023-06-10 12:28
Author: Jonathan Wiltshire
Status: published

The contents of bookworm are now being sent over to the Debian Images team for building and testing of all the .iso files you will be able to download when the release is complete #ReleasingDebianBookworm
