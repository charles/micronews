Title: The tested images have been signed off and the remaining release tasks can continue. Thank you all our image testers <3 #ReleasingDebianBookworm #Debian #Debian12
Slug: 1686424033
Date: 2023-06-10 19:07
Author: Jonathan Wiltshire
Status: published

The tested images have been signed off and the remaining release tasks can continue. Thank you all our image testers <3 #ReleasingDebianBookworm #Debian #Debian12
