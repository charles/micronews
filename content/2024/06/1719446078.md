Title: Warning: Debian 10 Long Term Support reaching end-of-life on Sunday June 30th https://lists.debian.org/debian-announce/2024/msg00001.html
Slug: 1719446078
Date: 2024-06-26 23:54
Author: Jean-Pierre Giraud
Status: published

Warning: Debian 10 Long Term Support reaching end-of-life on Sunday June 30th [https://lists.debian.org/debian-announce/2024/msg00001.html](https://lists.debian.org/debian-announce/2024/msg00001.html)
