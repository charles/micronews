Title: MiniDebConf in Berlin is coming up in four weeks! Registering now will guarantee that you'll get a t-shirt. We are still looking for presentations, as well as sponsors for the event. More information at https://berlin2024.mini.debconf.org/ 
Slug: 1713463534
Date: 2024-04-18 18:05
Author: Joost van Baal-Ilić
Status: published

MiniDebConf in Berlin is coming up in four weeks! Registering now will guarantee that you'll get a t-shirt. We are still looking for presentations, as well as sponsors for the event. More information at [https://berlin2024.mini.debconf.org/](https://berlin2024.mini.debconf.org/) 
