Title: MiniDebConf Belo Horizonte 2024 with presentations on Debian and Free Software, mostly in Brazilian Portuguese, is about to start in 2 weeks.  Registation is still possible and volunteers are very welcome; the schedule is now available.  See https://bh.mini.debconf.org/en/ 
Slug: 1713028893
Date: 2024-04-13 17:21
Author: Joost van Baal-Ilić
Status: published

MiniDebConf Belo Horizonte 2024 with presentations on Debian and Free Software, mostly in Brazilian Portuguese, is about to start in 2 weeks.  Registation is still possible and volunteers are very welcome; the schedule is now available.  See [https://bh.mini.debconf.org/en/](https://bh.mini.debconf.org/en/) 
