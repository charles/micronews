Title: As Debian Backports do not support Long Term Support, buster-backports is soon to be removed from the Debian Archive. https://lists.debian.org/debian-backports-announce/2024/04/msg00000.html #debian
Slug: 1713331251
Date: 2024-04-17 05:20
Author: Donald Norwood
Status: published

As Debian Backports do not support Long Term Support, buster-backports is soon to be removed from the Debian Archive. [https://lists.debian.org/debian-backports-announce/2024/04/msg00000.html](https://lists.debian.org/debian-backports-announce/2024/04/msg00000.html) #debian
