Title: Debian will participate in this year's December Outreachy round with the projects "Automatic Indi-3rd-party driver update" and "Migrate the Debian main website to HuGo".  We are currently in the Intern selection period and are looking forward to start the work.  See https://wiki.debian.org/Outreachy/ .  #debian #outreachy
Slug: 1728824072
Date: 2024-10-13 12:54
Author: Joost van Baal-Ilić
Status: published

Debian will participate in this year's December Outreachy round with the projects "Automatic Indi-3rd-party driver update" and "Migrate the Debian main website to HuGo".  We are currently in the Intern selection period and are looking forward to start the work.  See [https://wiki.debian.org/Outreachy/](https://wiki.debian.org/Outreachy/) .  #debian #outreachy
