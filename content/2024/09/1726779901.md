Title: Thank you to LWN.net for their recent article: Attracting and retaining Debian contributors, which highlights one of the ways that our project grows. https://lwn.net/Articles/987548/ #debian
Slug: 1726779901
Date: 2024-09-19 21:05
Author: Donald Norwood
Status: published

Thank you to LWN.net for their recent article: Attracting and retaining Debian contributors, which highlights one of the ways that our project grows. [https://lwn.net/Articles/987548/](https://lwn.net/Articles/987548/) #debian
