Title: We pause for lunch and will resume Day 3 of the  24th Debian Developers Conference at 2:30 PM (KST) #debian #debconf24 #busan #korea #debiankorea
Slug: 1722314780
Date: 2024-07-30 04:46
Author: Donald Norwod
Status: published

We pause for lunch and will resume Day 3 of the  24th Debian Developers Conference at 2:30 PM (KST) #debian #debconf24 #busan #korea #debiankorea
