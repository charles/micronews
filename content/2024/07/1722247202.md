Title: After the dinner, at 6:00 PM (KST) (09:00 UTC) don't forget to attend the performance Poetry Attack 01 and to participate at the traditional Cheese and Wine party in CEO https://debconf24.debconf.org/schedule/?block=9  #debian #debconf24 #busan #korea #debiankorea
Slug: 1722247202
Date: 2024-07-29 10:00
Author: Jean-Pierre Giraud
Status: published

After the dinner, at 6:00 PM (KST) (09:00 UTC) don't forget to attend the performance Poetry Attack 01 and to participate at the traditional Cheese and Wine party in CEO [https://debconf24.debconf.org/schedule/?block=9](https://debconf24.debconf.org/schedule/?block=9)  #debian #debconf24 #busan #korea #debiankorea
