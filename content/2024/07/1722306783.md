Title: Welcome back to our 3rd day of the 24th Debian Developers Conference held in Busan, Korea. Please check the the conference schedule: https://debconf24.debconf.org/schedule?block=10 for events and talks you would like to view. We will update before each talk and make announcements from this channel. Happy Debian to you!  #debian #debconf24 #busan #korea #debiankorea
Slug: 1722306783
Date: 2024-07-30 02:33
Author: Donald Norwood
Status: published

Welcome back to our 3rd day of the 24th Debian Developers Conference held in Busan, Korea. Please check the the conference schedule: [https://debconf24.debconf.org/schedule?block=10](https://debconf24.debconf.org/schedule?block=10) for events and talks you would like to view. We will update before each talk and make announcements from this channel. Happy Debian to you!  #debian #debconf24 #busan #korea #debiankorea
