Title: DebCamp starts in a couple days! Check out some of the sprints planned in https://wiki.debian.org/DebConf/24/Sprints
Slug: 1721447078
Date: 2024-07-20 03:44
Author: Carlos Henrique Lima Melara
Status: published

DebCamp starts in a couple days! Check out some of the sprints planned in [https://wiki.debian.org/DebConf/24/Sprints](https://wiki.debian.org/DebConf/24/Sprints)
