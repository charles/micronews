Title: Mini-DebConf Belo Horizonte (Brazil) 2024: Dates and Calls for Papers. Registration is now open. https://lists.debian.org/debian-devel-announce/2024/01/msg00002.html
Slug: 1705366359
Date: 2024-01-16 00:52
Author: Paulo Henrique de Lima Santana
Status: published

Mini-DebConf Belo Horizonte (Brazil) 2024: Dates and Calls for Papers. Registration is now open. [https://lists.debian.org/debian-devel-announce/2024/01/msg00002.html](https://lists.debian.org/debian-devel-announce/2024/01/msg00002.html)
