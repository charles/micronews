Title: Debian Project Leader Elections 2017: Call for nominations https://lists.debian.org/debian-devel-announce/2017/03/msg00000.html
Slug: 1488823288
Date: 2017-03-06 18:01
Author: Laura Arjona Reina
Status: published

Debian Project Leader Elections 2017: Call for nominations [https://lists.debian.org/debian-devel-announce/2017/03/msg00000.html](https://lists.debian.org/debian-devel-announce/2017/03/msg00000.html)
