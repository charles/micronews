Title: Updated Debian 9: 9.2 released https://www.debian.org/News/2017/20171007
Slug: 1507396409
Date: 2017-10-07 17:13
Author: Cédric Boutillier
Status: published

Updated Debian 9: 9.2 released [https://www.debian.org/News/2017/20171007](https://www.debian.org/News/2017/20171007)
