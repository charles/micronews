Title: You don't need to be an expert to contribute to Debian! Assisting users with problems on the user support lists is a large contribution to Debian. #releasingstretch
Slug: 1497771339
Date: 2017-06-18 07:35
Author: Cédric Boutillier
Status: published

You don't need to be an expert to contribute to Debian! Assisting users with problems on the user support lists is a large contribution to Debian. #releasingstretch
