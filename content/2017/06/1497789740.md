Title: Be sure to check out the upgrading advice for troubleshooting and warnings prior to your upgrade. https://www.debian.org/releases/stretch/amd64/release-notes/ch-upgrading.en.html #releasingstretch
Slug: 1497789740
Date: 2017-06-18 12:42
Author: Paul Wise
Status: published

Be sure to check out the upgrading advice for troubleshooting and warnings prior to your upgrade. [https://www.debian.org/releases/stretch/amd64/release-notes/ch-upgrading.en.html](https://www.debian.org/releases/stretch/amd64/release-notes/ch-upgrading.en.html) #releasingstretch
