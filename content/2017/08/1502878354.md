Title: "20170812-reproducible-policy (packages should build reproducibly - after 4 years this work of many is in debian-policy now)" by Holger Levsen http://layer-acht.org/thinking/blog/20170812-reproducible-policy/
Slug: 1502878354
Date: 2017-08-16 10:12
Author: Laura Arjona Reina
Status: published

"20170812-reproducible-policy (packages should build reproducibly - after 4 years this work of many is in debian-policy now)" by Holger Levsen [http://layer-acht.org/thinking/blog/20170812-reproducible-policy/](http://layer-acht.org/thinking/blog/20170812-reproducible-policy/)
