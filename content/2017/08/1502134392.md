Title: DebConf17: the Blends track continues with Debian Games, Debian Med BoF and Debian for Medical Software  https://debconf17.debconf.org/schedule/?day=2017-08-07
Slug: 1502134392
Date: 2017-08-07 19:33
Author: Laura Arjona Reina
Status: published

DebConf17: the Blends track continues with Debian Games, Debian Med BoF and Debian for Medical Software  [https://debconf17.debconf.org/schedule/?day=2017-08-07](https://debconf17.debconf.org/schedule/?day=2017-08-07)
