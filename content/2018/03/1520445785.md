Title: Want to run #Debian as a downloadable app on Windows 10 desktops? Check https://wiki.debian.org/InstallingDebianOn/Microsoft/Windows/SubsystemForLinux #WSL
Slug: 1520445785
Date: 2018-03-07 20:43
Author: Martin Zobel-Helas
Status: published

Want to run #Debian as a downloadable app on Windows 10 desktops? Check [https://wiki.debian.org/InstallingDebianOn/Microsoft/Windows/SubsystemForLinux](https://wiki.debian.org/InstallingDebianOn/Microsoft/Windows/SubsystemForLinux) #WSL
