Title: This afternoon at #DebConf18 starts with a plenary for everyone: "Multiple people", with Enrico Zini https://debconf18.debconf.org/talks/79-multiple-people/
Slug: 1533282423
Date: 2018-08-03 07:47
Author: Laura Arjona Reina
Status: published

This afternoon at #DebConf18 starts with a plenary for everyone: "Multiple people", with Enrico Zini [https://debconf18.debconf.org/talks/79-multiple-people/](https://debconf18.debconf.org/talks/79-multiple-people/)
