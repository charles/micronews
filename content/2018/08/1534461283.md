Title: Several Debian designers created artwork to celebrate #DebianDay #Debian25years - These are by Daniel Lenharo https://salsa.debian.org/lenharo/Contribution/tree/master/Deb25th
Slug: 1534461283
Date: 2018-08-16 23:14
Author: Laura Arjona Reina
Status: published

Several Debian designers created artwork to celebrate #DebianDay #Debian25years - These are by Daniel Lenharo [https://salsa.debian.org/lenharo/Contribution/tree/master/Deb25th](https://salsa.debian.org/lenharo/Contribution/tree/master/Deb25th)

![Debian is 25 years old by Daniel Lenharo ](|static|/images/Debian25years-DanielLenharo.png)
