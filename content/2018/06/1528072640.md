Title: "Looking for a new Raspberry Pi image maintainer" by Michael Stapelberg https://people.debian.org/~stapelberg//2018/06/03/raspi3-looking-for-maintainer.html
Slug: 1528072640
Date: 2018-06-04 00:37
Author: Laura Arjona Reina
Status: published

"Looking for a new Raspberry Pi image maintainer" by Michael Stapelberg [https://people.debian.org/~stapelberg//2018/06/03/raspi3-looking-for-maintainer.html](https://people.debian.org/~stapelberg//2018/06/03/raspi3-looking-for-maintainer.html)
