Title: Official call for Stretch artwork proposals for Debian 9.
Slug: 1465166954
Date: 2016-06-05 22:49:14
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/p7aumIBXQa-yrI5_sQZeAA

The official call for <a href="https://lists.debian.org/debian-devel-announce/2016/06/msg00001.html">Stretch artwork proposals</a>  has been made! Help define the look and feel of Debian 9.

