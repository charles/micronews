Title: Fourth DebConf16 Silver Sponsor: Bern University of Applied Sciences
Slug: 1454314534
Date: 2016-02-01 08:15:34
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/wIw2fYE2SuKjkASS0A6pdg

Thanks <a href="https://www.bfh.ch/">Bern University of Applied Sciences</a> for sponsoring DebConf16 at a Silver level! Do you want to <a href="http://debconf16.debconf.org/">sponsor</a> too?

