Title: Our second DebConf16 Gold Sponsor is Google
Slug: 1458292956
Date: 2016-03-18 09:22:36
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/GHXwxWWOT4C2159MPeaF-g

Thanks <a href="https://www.google.com/">Google</a>! Do you want to <a href="http://debconf16.debconf.org/">sponsor</a> too?
