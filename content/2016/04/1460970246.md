Title: Our second DebConf16 Bronze Sponsor is The Linux Foundation
Slug: 1460970246
Date: 2016-04-18 09:04:06
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/VzIziflOQZi-XdK2Oavjtg

Our second DebConf16 Bronze Sponsor is The Linux Foundation. Thanks <a href="http://www.linuxfoundation.org/">The Linux Foundation</a>! Do you want to <a href="https://debconf16.debconf.org">sponsor</a> too?
