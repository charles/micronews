Title: Bits from the Community Team, October 2020 https://lists.debian.org/debian-devel-announce/2020/10/msg00002.html
Slug: 1603450453
Date: 2020-10-23 10:54
Author: Laura Arjona Reina
Status: published

Bits from the Community Team, October 2020 [https://lists.debian.org/debian-devel-announce/2020/10/msg00002.html](https://lists.debian.org/debian-devel-announce/2020/10/msg00002.html)
