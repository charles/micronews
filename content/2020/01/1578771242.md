Title: Statistical analysis of the outcome of the GR "Init systems and systemd" by Rafael Laboissière https://github.com/rlaboiss/debian-gr-systemd-2019
Slug: 1578771242
Date: 2020-01-11 19:34
Author: Ana Guerrero López
Status: published

Statistical analysis of the outcome of the GR "Init systems and systemd" by Rafael Laboissière [https://github.com/rlaboiss/debian-gr-systemd-2019](https://github.com/rlaboiss/debian-gr-systemd-2019)
