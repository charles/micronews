Title: We continue with Day 2 coverage of MiniDebConf Brasil Online 2020. Today’s talks are in Portuguese with times posted UTC -3. Schedule of events: https://mdcobr2020.debian.net/schedule/ #Debian #MDCObr2020 #MDCOBR #MiniDebConf
Slug: 1606666392
Date: 2020-11-29 16:13
Author: Donald Norwood
Status: published

We continue with Day 2 coverage of MiniDebConf Brasil Online 2020. Today’s talks are in Portuguese with times posted UTC -3. Schedule of events: [https://mdcobr2020.debian.net/schedule/](https://mdcobr2020.debian.net/schedule/) #Debian #MDCObr2020 #MDCOBR #MiniDebConf
