Title: New Debian Developers and Maintainers (May and June 2020) https://bits.debian.org/2020/07/new-developers-2020-06.html
Slug: 1595369022
Date: 2020-07-21 22:03
Author: Laura Arjona Reina
Status: published

New Debian Developers and Maintainers (May and June 2020) [https://bits.debian.org/2020/07/new-developers-2020-06.html](https://bits.debian.org/2020/07/new-developers-2020-06.html)
