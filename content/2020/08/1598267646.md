Title: The second day of  #DebConf20 starts with "Building Linux distributions for fun and profit" at 10:00 UTC, after this at 11:00 UTC there will be the "Debian Science BoF" folllowed by "Where is Salsa CI right now?" at 12:00 UTC  https://debconf20.debconf.org/schedule/?block=2
Slug: 1598267646
Date: 2020-08-24 11:14
Author: urbec
Status: published

The second day of  #DebConf20 starts with "Building Linux distributions for fun and profit" at 10:00 UTC, after this at 11:00 UTC there will be the "Debian Science BoF" folllowed by "Where is Salsa CI right now?" at 12:00 UTC  [https://debconf20.debconf.org/schedule/?block=2](https://debconf20.debconf.org/schedule/?block=2)
