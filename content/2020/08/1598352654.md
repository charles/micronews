Title: The Debian Developers Conference continues today Tuesday 25 August, at 10:00 UTC - have a look at the schedule for the day #DebConf20 https://debconf20.debconf.org/schedule/?block=3
Slug: 1598352654
Date: 2020-08-25 10:50
Author: Laura Arjona Reina
Status: published

The Debian Developers Conference continues today Tuesday 25 August, at 10:00 UTC - have a look at the schedule for the day #DebConf20 [https://debconf20.debconf.org/schedule/?block=3](https://debconf20.debconf.org/schedule/?block=3)
