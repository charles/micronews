Title: There will be more #Debconf 20 events in Malayalam from Thursday to Saturday, see https://debconf20.debconf.org/schedule for time.
Slug: 1598553606
Date: 2020-08-27 18:40
Author: urbec
Status: published

There will be more #Debconf 20 events in Malayalam from Thursday to Saturday, see [https://debconf20.debconf.org/schedule](https://debconf20.debconf.org/schedule) for time.
