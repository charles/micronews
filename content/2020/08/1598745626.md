Title: After the last break of #DebConf20, enjoy the talk "The Power of Free" at 22:00 UTC https://debconf20.debconf.org/talks/38-the-power-of-free/
Slug: 1598745626
Date: 2020-08-30 00:00
Author: Donald Norwood
Status: published

After the last break of #DebConf20, enjoy the talk "The Power of Free" at 22:00 UTC [https://debconf20.debconf.org/talks/38-the-power-of-free/](https://debconf20.debconf.org/talks/38-the-power-of-free/)
