Title: At 11:00 UTC, at the same time as "Introduction and Overview of the Gaming ecosystem on GNU/Linux" (in Malayalam), there will be an ad-hoc sesion "Causal dynamics" (an approach to define a way to think algorithmic allowing natural and intuitive divergence into non linearity), in English. Links to video streams of both sessions from the #DebConf20 website https://debconf20.debconf.org/
Slug: 1598704861
Date: 2020-08-29 12:41
Author: Laura Arjona Reina
Status: published

At 11:00 UTC, at the same time as "Introduction and Overview of the Gaming ecosystem on GNU/Linux" (in Malayalam), there will be an ad-hoc sesion "Causal dynamics" (an approach to define a way to think algorithmic allowing natural and intuitive divergence into non linearity), in English. Links to video streams of both sessions from the #DebConf20 website [https://debconf20.debconf.org/](https://debconf20.debconf.org/)
