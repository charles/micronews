Title: All of the sessions for #DebConf20 for today have ended. Thank you to all of our contributors and viewers, and to our DebConf Video team! See you tomorrow: https://debconf20.debconf.org/schedule/?block=6
Slug: 1598580260
Date: 2020-08-28 02:04
Author: Donald Norwood
Status: published

All of the sessions for #DebConf20 for today have ended. Thank you to all of our contributors and viewers, and to our DebConf Video team! See you tomorrow: [https://debconf20.debconf.org/schedule/?block=6](https://debconf20.debconf.org/schedule/?block=6)
