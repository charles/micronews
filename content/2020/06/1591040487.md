Title: "Kotlin Update" by the Debian GSoC Kotlin project https://java-team.pages.debian.net/gsoc-kotlin-blog/2020/06/01/kotlin-update/
Slug: 1591040487
Date: 2020-06-01 19:41
Author: Laura Arjona Reina
Status: published

"Kotlin Update" by the Debian GSoC Kotlin project [https://java-team.pages.debian.net/gsoc-kotlin-blog/2020/06/01/kotlin-update/](https://java-team.pages.debian.net/gsoc-kotlin-blog/2020/06/01/kotlin-update/)
