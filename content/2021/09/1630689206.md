Title: Debian participates in the next Outreachy round. Let's work together, making free software, towards a more diverse community! Deadline for initial applications for interns is in 1h (3 Sept 16h UTC) Hurry up if you're elegible and still undecided! https://www.outreachy.org/
Slug: 1630689206
Date: 2021-09-03 17:13
Author: Laura Arjona Reina
Status: published

Debian participates in the next Outreachy round. Let's work together, making free software, towards a more diverse community! Deadline for initial applications for interns is in 1h (3 Sept 16h UTC) Hurry up if you're elegible and still undecided! [https://www.outreachy.org/](https://www.outreachy.org/)
