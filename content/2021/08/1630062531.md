Title: The fourth day of #DebConf21 starts! At 15:00 UTC, BoF of the Debian Android Tools team followed at 16:00 UTC by the talk "Summer with Debian", a project of 3-4 months long online mentorship program. In parallel, the workshop "Building virtual machines with KVM, QEMU and LibVirt" take place on channel two, https://debconf21.debconf.org/schedule/?block=4
Slug: 1630062531
Date: 2021-08-27 11:08
Author: Jean-Pierre Giraud
Status: published

The fourth day of #DebConf21 starts! At 15:00 UTC, BoF of the Debian Android Tools team followed at 16:00 UTC by the talk "Summer with Debian", a project of 3-4 months long online mentorship program. In parallel, the workshop "Building virtual machines with KVM, QEMU and LibVirt" take place on channel two, [https://debconf21.debconf.org/schedule/?block=4](https://debconf21.debconf.org/schedule/?block=4)
