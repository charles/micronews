Title: At 20:00 UTC, the Debian Hams BoF, a chance for the team and anyone else interested in radios/SDR to get together and talk about the current state of free software in amateur radio, packaging efforts, and more https://debconf21.debconf.org/schedule/venue/1/
Slug: 1630098058
Date: 2021-08-27 21:00
Author: Laura Arjona Reina
Status: published

At 20:00 UTC, the Debian Hams BoF, a chance for the team and anyone else interested in radios/SDR to get together and talk about the current state of free software in amateur radio, packaging efforts, and more [https://debconf21.debconf.org/schedule/venue/1/](https://debconf21.debconf.org/schedule/venue/1/)
