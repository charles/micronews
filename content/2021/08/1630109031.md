Title: On channel two, the Brazilian Portuguese track continues and closes today's sessions, with "O Profissional da Educação e o Debian: pelo uso de softwares livres nas escolas" at 23:00 UTC followed by "Hacker ou Maker: Uma analise a partir do contrato social do sistema operacional universal" at 00:00 UTC https://debconf21.debconf.org/schedule/venue/2/
Slug: 1630109031
Date: 2021-08-28 00:03
Author: Laura Arjona Reina
Status: published

On channel two, the Brazilian Portuguese track continues and closes today's sessions, with "O Profissional da Educação e o Debian: pelo uso de softwares livres nas escolas" at 23:00 UTC followed by "Hacker ou Maker: Uma analise a partir do contrato social do sistema operacional universal" at 00:00 UTC [https://debconf21.debconf.org/schedule/venue/2/](https://debconf21.debconf.org/schedule/venue/2/)
