Title: Visit our Debian virtual booth at #GitLabCommit - today 4 August at 18:00 CEST (16:00 UTC, 9:00 PDT) we'll have a live chat session https://gitlabcommitvirtual2021.com/
Slug: 1628088160
Date: 2021-08-04 14:42
Author: Laura Arjona Reina
Status: published

Visit our Debian virtual booth at #GitLabCommit - today 4 August at 18:00 CEST (16:00 UTC, 9:00 PDT) we'll have a live chat session [https://gitlabcommitvirtual2021.com/](https://gitlabcommitvirtual2021.com/)
