Title: Debian Edu / Skolelinux Bullseye — a complete Linux solution for your school https://www.debian.org/News/2021/20210815
Slug: 1629019802
Date: 2021-08-15 09:30
Author: Laura Arjona Reina
Status: published

Debian Edu / Skolelinux Bullseye — a complete Linux solution for your school [https://www.debian.org/News/2021/20210815](https://www.debian.org/News/2021/20210815)
