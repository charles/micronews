Title: Last activity in English for today at #DebConf21: 'Debian Electronics BOF': discuss recent changes in and the current state and future plans for packages that are maintained by the Debian Electronics team https://debconf21.debconf.org/schedule/venue/1/
Slug: 1630108605
Date: 2021-08-27 23:56
Author: Laura Arjona Reina
Status: published

Last activity in English for today at #DebConf21: 'Debian Electronics BOF': discuss recent changes in and the current state and future plans for packages that are maintained by the Debian Electronics team [https://debconf21.debconf.org/schedule/venue/1/](https://debconf21.debconf.org/schedule/venue/1/)
