Title:  #DebConf21 day 1 has ended, stay tuned tomorrow Wednesday 25, August 15:00 UTC. The schedule for the day is https://debconf21.debconf.org/schedule/?block=2 . Thank you to all of our contributors, viewers, and to our Debconf Video team! See you tomorrow!
Slug: 1629846006
Date: 2021-08-24 23:00
Author: Jean-Pierre Giraud
Status: published

 #DebConf21 day 1 has ended, stay tuned tomorrow Wednesday 25, August 15:00 UTC. The schedule for the day is [https://debconf21.debconf.org/schedule/?block=2](https://debconf21.debconf.org/schedule/?block=2) . Thank you to all of our contributors, viewers, and to our Debconf Video team! See you tomorrow!
