Title: Clojure Packaging team will discuss Clojure policy, prioritize applications to be introduced into Debian, and talk about open ITPs for Clojure packages in their BoF at 17:00 UTC on channel one, while Debian Science team holds an informal meeting on channel two, https://debconf21.debconf.org/schedule/?block=3
Slug: 1630003856
Date: 2021-08-26 18:50
Author: Jean-Pierre Giraud
Status: published

Clojure Packaging team will discuss Clojure policy, prioritize applications to be introduced into Debian, and talk about open ITPs for Clojure packages in their BoF at 17:00 UTC on channel one, while Debian Science team holds an informal meeting on channel two, [https://debconf21.debconf.org/schedule/?block=3](https://debconf21.debconf.org/schedule/?block=3)
