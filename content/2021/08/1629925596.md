Title: DebConf22 will be held in Kosovo. Follow their presentation and a BoF at 19:30 UTC to learn more about next DebConf organization, https://debconf21.debconf.org/schedule/?block=2
Slug: 1629925596
Date: 2021-08-25 21:06
Author: Jean-Pierre Giraud
Status: published

DebConf22 will be held in Kosovo. Follow their presentation and a BoF at 19:30 UTC to learn more about next DebConf organization, [https://debconf21.debconf.org/schedule/?block=2](https://debconf21.debconf.org/schedule/?block=2)
