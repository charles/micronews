Title: For bullseye, the security suite is now named bullseye-security so users should adapt their APT source-list files and configuration accordingly when upgrading. More details on https://www.debian.org/releases/bullseye/amd64/release-notes/ch-information.en.html#security-archive #NewInBullseye #Debian11Bullseye
Slug: 1629045283
Date: 2021-08-15 16:34
Author: Laura Arjona Reina
Status: published

For bullseye, the security suite is now named bullseye-security so users should adapt their APT source-list files and configuration accordingly when upgrading. More details on [https://www.debian.org/releases/bullseye/amd64/release-notes/ch-information.en.html#security-archive](https://www.debian.org/releases/bullseye/amd64/release-notes/ch-information.en.html#security-archive) #NewInBullseye #Debian11Bullseye
