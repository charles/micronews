Title: Debian 11 extends driverless printing to USB devices with the new "ipp-usb" package #NewInDebianBullseye #ReleasingDebianBullseye
Slug: 1628935419
Date: 2021-08-14 10:44
Author: Jonathan Wiltshire
Status: published

Debian 11 extends driverless printing to USB devices with the new "ipp-usb" package #NewInDebianBullseye #ReleasingDebianBullseye
