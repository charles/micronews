Title: Did you choose to use the KDE Plasma Desktop on Debian? The talk 'State of KDE/Plasma in Debian' is made for you at 23:00 UTC on channel one, https://debconf21.debconf.org/schedule/venue/1/
Slug: 1630024203
Date: 2021-08-27 00:30
Author: Laura Arjona Reina
Status: published

Did you choose to use the KDE Plasma Desktop on Debian? The talk 'State of KDE/Plasma in Debian' is made for you at 23:00 UTC on channel one, [https://debconf21.debconf.org/schedule/venue/1/](https://debconf21.debconf.org/schedule/venue/1/)
