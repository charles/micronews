Title: Debian sends love to all use, contribute, author, support, and make free software available to all. https://bits.debian.org/2021/02/ilovefs-2021.html Happy Free Software day! #ilovefs
Slug: 1613304398
Date: 2021-02-14 12:06
Author: Donald Norwood
Status: published

Debian sends love to all those who use, contribute, author, support, and make free software available to all. [https://bits.debian.org/2021/02/ilovefs-2021.html](https://bits.debian.org/2021/02/ilovefs-2021.html) Happy Free Software day! #ilovefs
