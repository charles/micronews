Title:  #MiniDebConf India 2021 is coming live on 23rd and 24th January (Sat/Sun). Schedule: https://in2021.mini.debconf.org/schedule Watch the events online in two venues Buzz & Rex #debian #freesoftware #DebianIndia #MDCO-IN-2021
Slug: 1611313329
Date: 2021-01-22 11:02
Author: Anupa Ann Joseph
Status: published

 #MiniDebConf India 2021 is coming live on 23rd and 24th January (Sat/Sun). Schedule: [https://in2021.mini.debconf.org/schedule](https://in2021.mini.debconf.org/schedule) Watch the events online in two venues Buzz & Rex #debian #freesoftware #DebianIndia #MDCO-IN-2021
