Title: Updated Debian 10: 10.11 released https://www.debian.org/News/2021/2021100902
Slug: 1633789991
Date: 2021-10-09 14:33
Author: Laura Arjona Reina
Status: published

Updated Debian 10: 10.11 released [https://www.debian.org/News/2021/2021100902](https://www.debian.org/News/2021/2021100902)
