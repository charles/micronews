Title: Debian is present at FOSDEM 2022! Visit our online stand during this weekend https://stands.fosdem.org/stands/debian/
Slug: 1644054285
Date: 2022-02-05 09:44
Author: Laura Arjona Reina
Status: published

Debian is present at FOSDEM 2022! Visit our online stand during this weekend [https://stands.fosdem.org/stands/debian/](https://stands.fosdem.org/stands/debian/)
