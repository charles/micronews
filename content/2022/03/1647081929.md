Title: Debian Reunion Hamburg 2022 from May 23 to 30 https://lists.debian.org/debian-events-eu/2022/03/msg00000.html
Slug: 1647081929
Date: 2022-03-12 10:45
Author: Laura Arjona Reina
Status: published

Debian Reunion Hamburg 2022 from May 23 to 30 [https://lists.debian.org/debian-events-eu/2022/03/msg00000.html](https://lists.debian.org/debian-events-eu/2022/03/msg00000.html)
