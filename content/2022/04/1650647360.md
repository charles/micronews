Title: New info about #DebConf22 venue and accommodation in its website, and reminder about submission deadlines (1st May) https://lists.debian.org/debconf-announce/2022/04/msg00000.html
Slug: 1650647360
Date: 2022-04-22 17:09
Author: Paulo Henrique de Lima Santana (phls)
Status: published

New info about #DebConf22 venue and accommodation in its website, and reminder about submission  deadlines (1st May) [https://lists.debian.org/debconf-announce/2022/04/msg00000.html](https://lists.debian.org/debconf-announce/2022/04/msg00000.html)
