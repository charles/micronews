Title: Please fix or triage RC bugs https://lists.debian.org/debian-devel/2022/07/msg00133.html
Slug: 1658097480
Date: 2022-07-17 22:38
Author: Laura Arjona Reina
Status: published

Please fix or triage RC bugs [https://lists.debian.org/debian-devel/2022/07/msg00133.html](https://lists.debian.org/debian-devel/2022/07/msg00133.html)
