Title: At the same time (13:00 UTC), the DebConf Committee will have a BoF (discussion session) in Ereniku room, also streamed: https://debconf22.debconf.org/schedule/venue/3/
Slug: 1658155600
Date: 2022-07-18 14:46
Author: Laura Arjona Reina
Status: published

At the same time (13:00 UTC), the DebConf Committee will have a BoF (discussion session) in Ereniku room, also streamed: [https://debconf22.debconf.org/schedule/venue/3/](https://debconf22.debconf.org/schedule/venue/3/)
