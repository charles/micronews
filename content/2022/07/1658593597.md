Title: Last set of short talks for today: at 15:00 UTC in Drini: "What is DPKG_ROOT? And what is it not?" followed by "What's New in GNOME 42". In Lumbardhi, "Next steps for HA in Debian" at 15:00 UTC followed by "Outreachy Project: Yarn plugin to resolve node modules installed as Debian packages via apt", and in Ereniku the Debian Hamradio BoF will happen from 15:00 UTC until 16:00 UTC https://debconf22.debconf.org/schedule/?block=7
Slug: 1658593597
Date: 2022-07-23 16:26
Author: Anupa Ann Joseph
Status: published

Last set of short talks for today: at 15:00 UTC in Drini: "What is DPKG_ROOT? And what is it not?" followed by "What's New in GNOME 42". In Lumbardhi, "Next steps for HA in Debian" at 15:00 UTC followed by "Outreachy Project: Yarn plugin to resolve node modules installed as Debian packages via apt", and in Ereniku the Debian Hamradio BoF will happen from 15:00 UTC until 16:00 UTC [https://debconf22.debconf.org/schedule/?block=7](https://debconf22.debconf.org/schedule/?block=7)
