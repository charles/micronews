Title: Today we start with "Introduction to setting up the Debian Packaging Development Environment" at 8:00 UTC in Drini, "Debian Brazil: What's going on with our local communities" in Lumbardhi, Debian Long Term Support BoF in Ereniku and Youth visit in Mirusha (no streaming)  https://debconf22.debconf.org/schedule/?block=6
Slug: 1658482104
Date: 2022-07-22 09:28
Author: Anupa Ann Joseph
Status: published

Today we start with "Introduction to setting up the Debian Packaging Development Environment" at 8:00 UTC in Drini, "Debian Brazil: What's going on with our local communities" in Lumbardhi, Debian Long Term Support BoF in Ereniku and Youth visit in Mirusha (no streaming)  [https://debconf22.debconf.org/schedule/?block=6](https://debconf22.debconf.org/schedule/?block=6)
