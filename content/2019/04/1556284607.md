Title: "An overview of Secure Boot in Debian" by Cyril Brulebois https://debamax.com/blog/2019/04/19/an-overview-of-secure-boot-in-debian
Slug: 1556284607
Date: 2019-04-26 13:16
Author: Laura Arjona Reina
Status: published

"An overview of Secure Boot in Debian" by Cyril Brulebois [https://debamax.com/blog/2019/04/19/an-overview-of-secure-boot-in-debian](https://debamax.com/blog/2019/04/19/an-overview-of-secure-boot-in-debian)
