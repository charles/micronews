Title: This weekend we have a #Debian Bug Squashing #Party in Bratislava, Slovakia https://wiki.debian.org/BSP/2019/02/sk/Bratislava #BSP
Slug: 1549467611
Date: 2019-02-06 15:40
Author: Laura Arjona Reina
Status: published

This weekend we have a #Debian Bug Squashing #Party in Bratislava, Slovakia [https://wiki.debian.org/BSP/2019/02/sk/Bratislava](https://wiki.debian.org/BSP/2019/02/sk/Bratislava) #BSP
