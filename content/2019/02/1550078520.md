Title: The next DIY #MiniDebCamp starts in two weeks in Laveno, Italy https://wiki.debian.org/DebianEvents/it/2019/SnowCamp #SnowCamp
Slug: 1550078520
Date: 2019-02-13 17:22
Author: Elena Grandi
Status: published

The next DIY #MiniDebCamp starts in two weeks in Laveno, Italy [https://wiki.debian.org/DebianEvents/it/2019/SnowCamp](https://wiki.debian.org/DebianEvents/it/2019/SnowCamp) #SnowCamp
