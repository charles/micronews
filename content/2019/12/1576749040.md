Title: Tails, the Debian Live-based system aimed at preserving your privacy and anonymity, celebrates their 10th anniversary. Congratulations! https://tails.boum.org/news/celebrating_10_years/
Slug: 1576749040
Date: 2019-12-19 09:50
Author: Laura Arjona Reina
Status: published

Tails, the Debian Live-based system aimed at preserving your privacy and anonymity, celebrates their 10th anniversary. Congratulations! [https://tails.boum.org/news/celebrating_10_years/](https://tails.boum.org/news/celebrating_10_years/)
