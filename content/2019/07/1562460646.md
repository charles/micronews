Title: Used for Debian events, booths, or as a banner of pride, Debian Flyers are available in 9 languages. You can help improve or translate it into other languages. https://debian.pages.debian.net/debian-flyers/ #ReleasingDebianBuster
Slug: 1562460646
Date: 2019-07-07 00:50
Author: Donald Norwood
Status: published

Used for Debian events, booths, or as a banner of pride, Debian Flyers are available in 9 languages. You can help improve or translate it into other languages. [https://debian.pages.debian.net/debian-flyers/](https://debian.pages.debian.net/debian-flyers/) #ReleasingDebianBuster
