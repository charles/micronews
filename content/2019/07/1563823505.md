Title: The afternoon at #DebConf19 continues with "Debian na vida de uma Operadora de Telecom", "Debian, enabling the developer use case on Chrome OS" and "Software transparency BoF" https://debconf19.debconf.org/schedule/?day=2019-07-22
Slug: 1563823505
Date: 2019-07-22 19:25
Author: Laura Arjona Reina
Status: published

The afternoon at #DebConf19 continues with "Debian na vida de uma Operadora de Telecom", "Debian, enabling the developer use case on Chrome OS" and "Software transparency BoF" [https://debconf19.debconf.org/schedule/?day=2019-07-22](https://debconf19.debconf.org/schedule/?day=2019-07-22)
