Title: Thanks to the Debian Video team, we can enjoy video streaming and recording on three rooms during #DebConf19. They also do training sessions for new volunteers (one of them will happen today) and coordinate with local and remote contributors to review and publish the recordings really fast!
Slug: 1563726718
Date: 2019-07-21 16:31
Author: Laura Arjona Reina
Status: published

Thanks to the Debian Video team, we can enjoy video streaming and recording on three rooms during #DebConf19. They also do training sessions for new volunteers (one of them will happen today) and coordinate with local and remote contributors to review and publish the recordings really fast!
