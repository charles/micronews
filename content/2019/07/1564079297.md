Title: In a few minutes: "FOSS Governance: The good, the bad and the ugly", "What's New in Postgres Land for Buster", "Yubikey hands on: how to create, store and use GPG subkeys stored on your Yubikey", "Latin America y Caribe Meeting (no streaming)"  #DebConf19  https://debconf19.debconf.org/schedule/?day=2019-07-25
Slug: 1564079297
Date: 2019-07-25 18:28
Author: Laura Arjona Reina
Status: published

In a few minutes: "FOSS Governance: The good, the bad and the ugly", "What's New in Postgres Land for Buster", "Yubikey hands on: how to create, store and use GPG subkeys stored on your Yubikey", "Latin America y Caribe Meeting (no streaming)"  #DebConf19  [https://debconf19.debconf.org/schedule/?day=2019-07-25](https://debconf19.debconf.org/schedule/?day=2019-07-25)
