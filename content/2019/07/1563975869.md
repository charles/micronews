Title: Thanks Aigars Mahinovs, the #DebConf19 group photo is already available https://wiki.debian.org/DebConf/19/Photos
Slug: 1563975869
Date: 2019-07-24 13:44
Author: Laura Arjona Reina
Status: published

Thanks Aigars Mahinovs, the #DebConf19 group photo is already available [![DebConf19 group photo](|static|/images/debconf19_group_small.jpg)](https://wiki.debian.org/DebConf/19/Photos)
