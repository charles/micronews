Title: Afternoon at #DebConf19: Debian Cloud Team BoF and short talks: "debootstrap update" and "Cluster NFS", followed by "APT 2.0 and other news" and "InterIMAP — The case for local IMAP servers and fast bidirectional synchronization" https://debconf19.debconf.org/schedule/?day=2019-07-22
Slug: 1563819929
Date: 2019-07-22 18:25
Author: Laura Arjona Reina
Status: published

Afternoon at #DebConf19: Debian Cloud Team BoF and short talks: "debootstrap update" and "Cluster NFS", followed by "APT 2.0 and other news" and "InterIMAP — The case for local IMAP servers and fast bidirectional synchronization" [https://debconf19.debconf.org/schedule/?day=2019-07-22](https://debconf19.debconf.org/schedule/?day=2019-07-22)
