Title: Debian is welcoming applicants for #Outreachy and #GSoC 2019 https://bits.debian.org/2019/03/call-for-applicants-outreachy-gsoc-2019.html
Slug: 1553775564
Date: 2019-03-28 12:19
Author: Laura Arjona Reina
Status: published

Debian is welcoming applicants for #Outreachy and #GSoC 2019 [https://bits.debian.org/2019/03/call-for-applicants-outreachy-gsoc-2019.html](https://bits.debian.org/2019/03/call-for-applicants-outreachy-gsoc-2019.html)
