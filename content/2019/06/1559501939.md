Title: The Marseille mini-DebConf 2019 videos have been published: https://meetings-archive.debian.net/pub/debian-meetings/2019/miniconf-marseille/
Slug: 1559501939
Date: 2019-06-02 18:58
Author: Louis-Philippe Véronneau
Status: published

The Marseille mini-DebConf 2019 videos have been published: [https://meetings-archive.debian.net/pub/debian-meetings/2019/miniconf-marseille/](https://meetings-archive.debian.net/pub/debian-meetings/2019/miniconf-marseille/)
