Title: Please welcome the second DebConf15 Gold sponsor: sipgate
Slug: 1415828549
Date: 2014-11-12 21:42:29
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/VRuvHbOeTeqz3nC11qqdtA

Please welcome the second <a href="http://debconf15.debconf.org/">DebConf15</a> Gold sponsor: <a href="http://sipgate.de">sipgate</a>! <a href="http://debconf15.debconf.org/become-sponsor.xhtml">Want to sponsor too</a>?

