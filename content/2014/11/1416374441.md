Title: Laura Arjona shows how to contribute to Debian package descriptions in other languages.
Slug: 1416374441
Date: 2014-11-19 05:20:41
Author: imported from identi.ca/debian
Status: published
identica: https://identi.ca/debian/note/ZqeWp-d3Qz2Xff42BO-T3A

Laura Arjona <a href="http://larjona.wordpress.com/2014/11/19/translating-reviewing-debian-package-descriptions/">shows</a> how to contribute to Debian package descriptions in other languages.

