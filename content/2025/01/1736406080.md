Title: Our map of Developer Locations now links to a nice interactive world map of places where Debian Developers offer PGP keysigning which we use to prove the identities of people who wish to get involved in the Debian project.  Debian Members can add themselves to the PGP signing offers wiki page and soon you will be listed at https://www.debian.org/devel/developers.loc #debian #community
Slug: 1736406080
Date: 2025-01-09 07:01
Author: Joost van Baal-Ilić
Status: published

Our map of Developer Locations now links to a nice interactive world map of places where Debian Developers offer PGP keysigning, which we use to prove the identities of people who wish to get involved in the Debian project.  Debian Members can add themselves to the PGP signing offers wiki page and soon you will be listed at [https://www.debian.org/devel/developers.loc](https://www.debian.org/devel/developers.loc) #debian #community


